package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.devcamp.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    public ArrayList<Invoice> getAllInvoice(){
        Invoice invoice1 = new Invoice(1, customerService.customer1, 10000);
        Invoice invoice2 = new Invoice(2, customerService.customer2, 10000);
        Invoice invoice3 = new Invoice(3, customerService.customer3, 7000);

        Invoice invoice4 = new Invoice(4, customerService.customer3, 10000);
        Invoice invoice5 = new Invoice(5, customerService.customer2, 15000);
        Invoice invoice6 = new Invoice(6, customerService.customer3, 2000);
    
        ArrayList<Invoice> allInvoice = new ArrayList<>();
        allInvoice.add(invoice1);
        allInvoice.add(invoice2);
        allInvoice.add(invoice3);
        allInvoice.add(invoice4);
        allInvoice.add(invoice5);
        allInvoice.add(invoice6);

        return allInvoice;
    }

    public Invoice getInvoiceByIndex(@PathVariable("invoiceId") int index) {
        ArrayList<Invoice> allInvoices = getAllInvoice();
        if (index >= 0 && index < allInvoices.size()) {
            return allInvoices.get(index);
        } else {
            return null;
        }
    }

}
